// ctime is the only CPP part of this file...

// standard bibliotheken
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <ctime>

// Lua und das OpenVG bibliotheken
#include "lua5.2/lua.hpp"
extern "C" {
	#include "VG/openvg.h"
	#include "VG/vgu.h"
	#include "openvg/fontinfo.h"
	#include "openvg/shapes.h"
}

using namespace std;

// globale variablen
lua_State *L;
int width, height, sleepTime;

/* assume that table is on the stack top */
int getfieldint(const char *key) {
	int result;
	lua_pushstring(L, key);
	lua_gettable(L, -2);  /* get background[key] */
	if (!lua_isnumber(L, -1))
		luaL_error(L, "invalid number");
	result = (int)lua_tonumber(L, -1);
	lua_pop(L, 1);  /* remove number */
	return result;
}

/* assume that table is at the top */
void setfieldint (const char *index, int value) {
	lua_pushstring(L, index);
	lua_pushnumber(L, value);
	lua_settable(L, -3);
}

void callLuaRender(float deltaTime)
{
	lua_getglobal(L, "render");
	lua_pushnumber(L, deltaTime);
	if(lua_pcall(L, 1, 0, 0) != 0)
		luaL_error(L, "Cannot run render function! Error: %s", lua_tostring(L, -1));
}

void setinitialtable()
{
	lua_newtable(L);
	setfieldint("fps", 60);
	lua_setglobal(L, "game");
}

static int invokeDrawText(lua_State* L)
{
	double x = luaL_checknumber(L, 1);
	double y = luaL_checknumber(L, 2);
	char* text = (char*)luaL_checkstring(L, 3);
	int size = luaL_checknumber(L, 4);
	int orien = luaL_checknumber(L, 5);
	switch(orien)
	{
	case 1:
		TextMid(x, y, text, SerifTypeface, size);
		break;
	case 2:
		TextEnd(x, y, text, SerifTypeface, size);
		break;
	default:
		Text(x, y, text, SerifTypeface, size);
	}

	return 0;
}

static int invokeFill(lua_State* L)
{
	double r = luaL_checknumber(L, 1);
	double g = luaL_checknumber(L, 2);
	double b = luaL_checknumber(L, 3);
	double a = luaL_checknumber(L, 4);
	Fill(r,g,b,a);
	return 0;
}

int main() {

	// initialisieren
	struct timeval begin, end;
	char s[3];
	init(&width, &height);		
	int framesElapsed = 0;
	int frameTicker = 0;
	int fps = 0;
	int deltaTime = 1;
	
	// konfiguration
	L = luaL_newstate();
	luaL_openlibs(L);
	lua_pushcfunction(L, invokeDrawText);
	lua_setglobal(L, "drawtext");
	lua_pushcfunction(L, invokeFill);
	lua_setglobal(L, "fill");
	if(luaL_dofile(L, "init.lua"))
	{
		luaL_error(L, "Failed to run init.lua!: %s", lua_tostring(L, -1));
		exit(1);
	}

	// initialisieren lua Konfiguration
	lua_getglobal(L, "config");
	if(!lua_istable(L, -1))
	{
		luaL_error(L, "'config' table does not exist or is invalid!");
		exit(1);
	}
	sleepTime = getfieldint("cpuSleepTime");
	setinitialtable();

	while(1)
	{
		gettimeofday(&begin, NULL);

		framesElapsed = framesElapsed + 1;
		usleep(sleepTime);
		Start(width, height);
		Background(0, 0, 0);	
		time_t currentTime = time(0);
		struct tm* now = localtime(&currentTime);
		lua_getglobal(L, "game");
		setfieldint("time_hour", now->tm_hour);
		setfieldint("time_minute", now->tm_min);
		setfieldint("time_second", now->tm_sec);
		setfieldint("width", width);
		setfieldint("height", height);
		callLuaRender(deltaTime);

		End();
		gettimeofday(&end, NULL);
		deltaTime = (end.tv_usec - begin.tv_usec) / 1000;

	}
	fgets(s, 2, stdin);				   
	finish();
	lua_close(L);
	exit(0);
}
