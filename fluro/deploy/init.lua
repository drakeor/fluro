-- Set how many microseconds we want the CPU to sleep between frames
print "Loading config!"

config = {
	cpuSleepTime = 800,
}

function render(deltaTime)
	fill(255,255,255,1)
	drawtext(game.width/2, game.height/2, game.time_hour .. ":" .. game.time_minute .. ":" .. game.time_second, 72, 1)
end
