################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../fluro/openvg/font2openvg.cpp 

C_SRCS += \
../fluro/openvg/libshapes.c \
../fluro/openvg/oglinit.c 

OBJS += \
./fluro/openvg/font2openvg.o \
./fluro/openvg/libshapes.o \
./fluro/openvg/oglinit.o 

C_DEPS += \
./fluro/openvg/libshapes.d \
./fluro/openvg/oglinit.d 

CPP_DEPS += \
./fluro/openvg/font2openvg.d 


# Each subdirectory must supply rules for building sources it contributes
fluro/openvg/%.o: ../fluro/openvg/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

fluro/openvg/%.o: ../fluro/openvg/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


